import * as React from 'react';

interface ButtonProps {
  label: string;
  action: () => void;
  disabled?: boolean;
}

class Button extends React.Component<ButtonProps, {}> {
  render() {
    const { disabled, action, label } = this.props;

    const classes = 'action ' + (disabled ? 'disabled' : 'enabled');

    return (
      <div className={classes} onClick={action} >
        {label}
      </div>
    );
  };
};

export default Button;
