import * as React from 'react';

const colors = {
  ForestGreen: '#228B22',
  Snow: '#FFFAFA',
  SlateGray: '#708090',
  Sapphire: '#2F519E',
  Malachite: '#0BDA51',
  Starship: '#ECF245',
}

interface WorldMapProps {
  map: IMapData;
  player: IPlayer;
}

class WorldMap extends React.Component<WorldMapProps, {}> {
  render() {
    const tiles = this.props.map.tiles;
    const player = this.props.player;
    const px = player.coords.x;
    const py = player.coords.y;
    const mapTiles: Array<JSX.Element> = [];
    tiles.forEach((x, xk) => {
      const mapRow: Array<JSX.Element> = [];
      x.forEach((y, yk) => {
        const playerHere = (py === xk) && (px === yk);

        const color = tileColor(tiles[xk][yk], playerHere);

        const style = { backgroundColor: color };

        mapRow.push(
          <span key={xk + '-' + yk} className='maptile' style={style}>
          </span>
        );
      })

      mapTiles.push(
        <div key={xk} className='maptile-row'>
          {mapRow}
        </div>
      );
    })
    return (
      <div>
        <div className='world-map'>
          {mapTiles}
        </div>
      </div>
    );
  }
}

function tileColor(currentTile: number, playerHere: boolean) {
  return playerHere ? colors.Starship :
    currentTile > 45 ? colors.ForestGreen :
    currentTile > 5 ? colors.Malachite :
    currentTile > -10 ? colors.Sapphire :
    currentTile > -50 ? colors.SlateGray :
    colors.Snow;
}

export default WorldMap;
export { colors };
