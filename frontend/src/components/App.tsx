import { createSelector } from 'reselect'
import * as React from 'react';
import { connect } from 'react-redux';

import ResourceList from './ResourceList';
import ButtonList from './ButtonList';
import Map from './Map';

interface AppProps {
  resources: IResources;
  map: IMapData;
  cell: number;
  player: IPlayer;
  wandern: () => void;
  wandere: () => void;
  wanders: () => void;
  wanderw: () => void;
  gather_wood: () => void;
  light_fire: () => void;
  followers: IFollowers;
  time: ITime;
}

class App extends React.Component<AppProps, {}> {
  render() {
    return (
      <div>
        <ResourceList
          res={this.props.resources}
          cell={this.props.cell}
          followers={this.props.followers}
          time={this.props.time}
        />
        <ButtonList
          res={this.props.resources}
          cell={this.props.cell}
          wandern={this.props.wandern}
          wandere={this.props.wandere}
          wanders={this.props.wanders}
          wanderw={this.props.wanderw}
          gather_wood={this.props.gather_wood}
          light_fire={this.props.light_fire}
        />
        <Map
          map={this.props.map}
          player={this.props.player}
        />
      </div>
    );
  }
}

const mapSelector = (state: IGameState) => state.map
const playerSelector = (state: IGameState) => state.player

const mapTileSelector = createSelector(
  mapSelector,
  playerSelector,
  (map, player) => map.tiles[player.coords.x][player.coords.y]
);

function stateSelector(state: any) {
  return Object.assign({},
    state,
    { cell: mapTileSelector(state) }
  );
}

const mapDispatchToProps = (dispatch: Function) => {
  return {
    wandern:     () => { dispatch({ type: 'WANDERN' }) },
    wandere:     () => { dispatch({ type: 'WANDERE' }) },
    wanders:     () => { dispatch({ type: 'WANDERS' }) },
    wanderw:     () => { dispatch({ type: 'WANDERW' }) },
    gather_wood: () => { dispatch({ type: 'GATHER_WOOD' }) },
    light_fire:  () => { dispatch({ type: 'LIGHT_FIRE' }) },
  }
}

export default connect(stateSelector, mapDispatchToProps)(App);
