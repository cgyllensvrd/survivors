import * as React from 'react';
import Button from './Button';

interface IResource {
  amount: number;
}

interface ResList {
  wood: IResource;
}

interface ButtonListProps {
  res: ResList;
  cell: number;
  wandern: () => void;
  wandere: () => void;
  wanders: () => void;
  wanderw: () => void;
  gather_wood: () => void;
  light_fire: () => void;
}

class ButtonList extends React.Component<ButtonListProps, {}> {
  render() {
    const { res, cell } = this.props;
    const { wandern, wandere, wanders, wanderw } = this.props;
    const { gather_wood, light_fire } = this.props;

    const buttons: Array<JSX.Element> = [];

    buttons.push(<Button key='0' label='Gather wood' action={gather_wood} disabled={cell <= 45}/>)

    if (res.wood.amount >= 500) {
      buttons.push(<Button key='1' label='Light fire' action={light_fire} />)
    }

    buttons.push(<Button key='2' label='Wander N' action={wandern} />)
    buttons.push(<Button key='3' label='Wander E' action={wandere} />)
    buttons.push(<Button key='4' label='Wander S' action={wanders} />)
    buttons.push(<Button key='5' label='Wander W' action={wanderw} />)

    return (
      <div className='button-list'>
        {buttons}
      </div>
    );
  }
};

export default ButtonList;
