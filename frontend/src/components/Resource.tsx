import * as React from 'react';

interface ResProps {
  label: string;
  value: number;
}

class Resource extends React.Component<ResProps, {}> {
  render() {
    const { value, label } = this.props;
    return (
      <div className="resource">
        {label}: {value}
      </div>
    );
  }
};

export default Resource;
