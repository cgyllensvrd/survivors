import * as React from 'react';
import Resource from './Resource';

class ResourceList extends React.Component<ResourceListProps, {}> {
  render() {
    const { res, followers, cell, time } = this.props;
    const resources: Array<JSX.Element> = [];

    Object.keys(res).forEach((value, key) => {
      const cres = res[value];
      resources.push(<Resource key={value} label={value} value={Math.round(cres.amount) / 100} />);
    });

    const trees = cell > 45 ? cell - 45 : 0;
    resources.push(<Resource key='trees' label='Trees' value={Math.round(trees)} />);

    if (followers !== undefined) {
      resources.push(<Resource key='followers' label='Followers' value={followers.count} />);
    }

    resources.push(<Resource key='time' label='Day' value={time.day} />);

    return (
      <div className='resource-list'>
        {resources}
      </div>
    );
  }
};

export default ResourceList;
