import { createStore, /*applyMiddleware*/ } from 'redux';
import nextState from './util/nextState';
// import createLogger from 'redux-logger';

const store = createStore(
  nextState
// , applyMiddleware(createLogger())
);

// @TODO Store the interval id somewhere so we can access it, say for pausing.
// @TODO Implement pause.
// @TODO Make tick frequency configurable? Ensure combat (in multiplayer?)
//   isn't negatively affected.
/*const TickIntervalID =*/ window.setInterval(() => {
  store.dispatch({ type: 'TICK' });
}, 200);

/*const SaveIntervalID =*/ window.setInterval(() => {
  const save = JSON.stringify(store.getState());
  window.localStorage.setItem('save', save);
}, 5000);

// window.store = store;
export default store;
