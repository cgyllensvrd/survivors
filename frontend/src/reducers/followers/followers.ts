import saveHandler from '../../util/saveHandler';
import { Decoder, at, object, number } from 'type-safe-json-decoder'

export default function followers(state: IFollowers, resources: IResources, action: FSAction): IFollowers {
  switch (action.type) {
  case 'TICK':
    if (resources.safety.amount < 10 && state.count > 0) {
      return Object.assign({}, state, {
        count: 0,
      });
    }
    else if (resources.safety.amount >= 10 && state.count < 1) {
      return Object.assign({}, state, {
        count: 1,
      });
    }
    break;
  }

  return state;
}


export function initialFollowers() {
  return Object.assign({ count: 0 });
}

export function savedFollowers() {
  return saveHandler(initialFollowers(), followerDecoder, 'followers');
}
interface IFollowers {
  count: number,
}

const followerDecoder: Decoder<IFollowers> = at(['followers'],
  object(
    ['count', number()],
    (count) => ({count})
  )
)
