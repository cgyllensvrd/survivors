import * as expect from 'expect'
import followers, { initialFollowers } from './followers';
import { initialResources } from '../resources/resources';
import * as mocha from 'mocha'

describe('followers reducer', () => {
  const initF = initialFollowers();
  const initR = initialResources();
  const tick = { type: 'TICK' };

  it('should increase the number of followers to 1 when safety gets to 10+', () => {
    let res = Object.assign({}, initR, { safety: { amount: 9 } });

    let followerState = followers(initF, res, tick);
    expect(followerState).toEqual({ count: 0 });

    res = Object.assign({}, initR, { safety: { amount: 10 } });
    followerState = followers(initF, res, tick);
    expect(followerState).toEqual({ count: 1 });

    followerState = followers(initF, res, tick);
    expect(followerState).toEqual({ count: 1 });
  });

  it('should lower the number of followers to 0 if safety drops below 10', () => {
    let res = Object.assign({}, initR, { safety: { amount: 10 } });
    let followerState = followers(initF, res, tick);
    expect(followerState).toEqual({ count: 1 });

    res = Object.assign({}, initR, { safety: { amount: 9 } });
    followerState = followers(initF, res, tick);
    expect(followerState).toEqual({ count: 0 });
  });
});
