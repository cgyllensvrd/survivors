import saveHandler from '../../util/saveHandler';
import { Decoder, at, object, number } from 'type-safe-json-decoder'

// Number of milliseconds in a day.
const dayLength = 10000;

export default function(timeState: ITime, now: number, action: FSAction): ITime {
  const timePassed = now - timeState.msecForLastDayIncrement;
  if (timePassed >= dayLength) {
    const nextDayPart = timePassed % dayLength;
    return {
      day: timeState.day + 1,
      msecForLastDayIncrement: now - nextDayPart,
    };
  }
  return timeState;
}

export function savedTime(): ITime {
  return saveHandler(initialTime(), timeDecoder, 'time');
}

export function initialTime() {
  return Object.assign({
    day: 0,
    msecForLastDayIncrement: new Date().getTime(),
  });
}

export {
  dayLength,
};

const timeDecoder: Decoder<ITime> = at(['time'],
  object(
    ['day', number()],
    ['msecForLastDayIncrement', number()],
    (day, msecForLastDayIncrement) => ({day, msecForLastDayIncrement})
  )
)
