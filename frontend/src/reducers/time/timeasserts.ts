import time, { savedTime, initialTime, dayLength } from './time';
import assert from '../../util/assert';

export default function(timeState: ITime, now: number, action: FSAction): ITime {
  const newState = time(timeState, now, action);
  if (now - timeState.msecForLastDayIncrement >= dayLength) {
    assert(newState.day - timeState.day === 1, JSON.stringify([timeState, newState, 'Expected to advance one day' ]));
  }
  return newState;
}

// Re-export to maintain import interface.
export {
  savedTime,
  initialTime,
  dayLength,
};
/*describe('time reducer', () => {
  it('should increase day after each full day length', () => {
    const now = initialTime().msecForLastDayIncrement;
    const tick = { type: 'TICK' };

    let timeState: ITime = time(initialTime(),  now + (dayLength - 1), tick);
    expect(timeState).toEqual({
      day: 0,
      msecForLastDayIncrement: now,
    });

    timeState = time(timeState, now + (dayLength + 1), tick)
    expect(timeState).toEqual({
      day: 1,
      msecForLastDayIncrement: now + dayLength,
    });

    timeState = time(timeState, now + (dayLength * 2), tick)
    expect(timeState).toEqual({
      day: 2,
      msecForLastDayIncrement: now + (dayLength * 2),
    });
  });

  it('should not allow the number of day to run away after a long pause', () => {
    const now = initialTime().msecForLastDayIncrement;
    const tick = { type: 'TICK' };

    let timeState: ITime = time(initialTime(),  now + (dayLength - 1), tick);
    expect(timeState).toEqual({
      day: 0,
      msecForLastDayIncrement: now,
    });

    for (let i = 0; i < 2; i++) {
      timeState = time(timeState, now + (dayLength * 1000), tick)
      expect(timeState).toEqual({
        day: 1,
        msecForLastDayIncrement: now + (dayLength * 1000),
      });
    }
  });
});*/
