import resources, { initialResources } from './resources'
import { initialPlayer }    from '../player/player'
import { initialFollowers } from '../followers/followers'
import GameMap from '../../util/MapGen';
import * as expect from 'expect';

describe('resources reducer', () => {
  it('should handle LIGHT_FIRE', () => {
    expect(
      resources(initialResources(), new GameMap().generateMap(), initialPlayer(),initialFollowers(), {
        type: 'LIGHT_FIRE',
      })
    ).toEqual({
      wood:     { amount: -500 },
      safety:   { amount: 500  },
      prestige: { amount: 0  },
    });
  });
});
