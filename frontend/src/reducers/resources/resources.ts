import saveHandler from '../../util/saveHandler';
import { Decoder, at, object, number } from 'type-safe-json-decoder'

export default function resources(state: IResources, map: IMapData, player: IPlayer, followers: IFollowers, action: FSAction) {
  const currentTile = map.tiles[player.coords.x][player.coords.y];

  switch (action.type) {
  case 'LIGHT_FIRE':
    return Object.assign({}, state, {
      safety: { amount: state.safety.amount + 500 },
      wood:   { amount: state.wood.amount - 500 },
    })
  case 'GATHER_WOOD':
    return Object.assign({}, state, {
      wood:  { amount: state.wood.amount + 100 },
    })
  case 'TICK':
    // @TODO Redefine tick to work on "time since last tick".
    let newState = Object.assign({}, state);
    newState.safety = { amount: state.safety.amount - 1 }
    if (currentTile > 45 && followers.count > 0) {
      newState.wood = { amount: state.wood.amount + 100 * followers.count }
    }
    return newState;
  }

  return state;
}

export function initialResources() {
  return Object.assign({
    wood:   { amount: 0 },
    safety: { amount: 0 },
    prestige: { amount: 0 },
  });
}

export function savedResources() {
  return saveHandler(initialResources(), resourcesDecoder, 'resources');
}

const resourcesDecoder: Decoder<IResources> = at(['resources'],
  object(
    ['wood', object(
      ['amount', number()],
      (amount) => ({amount})
    )],
    ['safety', object(
      ['amount', number()],
      (amount) => ({amount})
    )],
    ['prestige', object(
      ['amount', number()],
      (amount) => ({amount})
    )],
    (wood, safety, prestige) => ({wood, safety, prestige})
  )
)
