import GameMap from '../util/MapGen';

export default function map(state: IMapData, player: IPlayer, followers: IFollowers, action: FSAction) {
  const newMap: IMapData = Object.assign({}, state);
  const currentTile = state.tiles[player.coords.x][player.coords.y];

  if (action.type === 'GATHER_WOOD') {
    newMap.tiles[player.coords.x][player.coords.y] -= 0.5;
    return newMap;
  }
  if (action.type === 'TICK') {
    // Forest growth.
    newMap.tiles = state.tiles.map(row => row.map(tile => tile > 45 ? tile + 0.01 : tile));

    if (followers.count > 0 && currentTile > 45) {
      // Logging.
      newMap.tiles[player.coords.x][player.coords.y] -= 0.05 * followers.count;
    }
    return newMap;
  }

  return state;
}

export function savedMap(): IMapData {
  // The common load function isn't used because we don't want to generate an
  // expensive new map unless we have to.
  try {
    const saveData = window.localStorage.getItem('save');
    if (saveData === null) {
      return new GameMap().generateMap();
    }
    let save: IGameState = JSON.parse(saveData);
    return save.map;
  } catch (e) {
    return new GameMap().generateMap();
  }
}
