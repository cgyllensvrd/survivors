import * as expect from 'expect'
import reset  from './reset';
import { initialGameState }  from '../../util/nextState';

describe('reset reducer', () => {
  it('should reset game state to initial conditions when safety is below -100, but increase prestige', () => {
    const tick = { type: 'TICK' };
    let testState = initialGameState();
    testState.resources.safety.amount = -101;

    let resetState = reset(testState, tick);

    expect(resetState.resources.safety.amount).toEqual(0);
    expect(resetState.resources.prestige.amount).toEqual(1);

    resetState.resources.safety.amount = -101;
    resetState = reset(resetState, tick);

    expect(resetState.resources.safety.amount).toEqual(0);
    expect(resetState.resources.prestige.amount).toEqual(2);

    testState = initialGameState();
    testState.resources.safety.amount = -100;

    resetState = reset(testState, tick);

    expect(resetState.resources.safety.amount).toEqual(-100);
    expect(resetState.resources.prestige.amount).toEqual(0);
  });
});
