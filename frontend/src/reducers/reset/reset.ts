import { initialGameState } from '../../util/nextState';

export default function reset(state: IGameState, action: FSAction): IGameState {
  switch (action.type) {
  case 'TICK':
    if (state.resources.safety.amount < -500) {
      const resetState = initialGameState()
      resetState.resources.prestige.amount = state.resources.prestige.amount + 1
      return resetState;
    }
    break;
  }

  return state;
}
