import saveHandler from '../../util/saveHandler';
import { Decoder, at, object, number } from 'type-safe-json-decoder'

function newCoords(x: number, y: number) {
  return {
    coords: { x, y },
  };
}

export default function player(state: IPlayer, action: FSAction) {
  const { x, y } = state.coords;
  switch (action.type) {
    case 'WANDERN':
      return newCoords(x, y - 1);
    case 'WANDERE':
      return newCoords(x + 1, y);
    case 'WANDERS':
      return newCoords(x, y + 1);
    case 'WANDERW':
      return newCoords(x - 1, y);
  }

  return state;
}


export function initialPlayer() {
  return Object.assign({
    coords: {x: 6, y: 6},
  })
}

export function savedPlayer(): IPlayer {
  return saveHandler(initialPlayer(), playerDecoder, 'player');
}

// @TODO Can this data be gathered as a reflection of the typescript interface
// we're decoding into? Reflected or not, what are the consequences to save-game
// version upgrades?
const playerDecoder: Decoder<IPlayer> = at(['player'],
  object(
    ['coords', object(
      ['x', number()],
      ['y', number()],
      (x, y) => ({x, y})

    )],
    (coords) => ({coords})
  )
)
