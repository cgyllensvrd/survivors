import resources, { savedResources, initialResources } from '../reducers/resources/resources';
import followers, { savedFollowers, initialFollowers } from '../reducers/followers/followers';
import time,      { savedTime,      initialTime }      from '../reducers/time/timeasserts';
import player,    { savedPlayer,    initialPlayer }    from '../reducers/player/player';
import map,       { savedMap } from '../reducers/map';
import GameMap from './MapGen';
import reset   from '../reducers/reset/reset';

export default function nextState(state: IGameState = savedGameState(), action: FSAction) {
  const now = new Date().getTime();

  let newState:IGameState = {
    resources: resources(state.resources, state.map, state.player, state.followers, action),
    map: map(state.map, state.player, state.followers, action),
    player: player(state.player, action),
    followers: followers(state.followers, state.resources, action),
    time: time(state.time, now, action),
  };

  // Reducers that depend on the new state, rather than the previous one
  newState = reset(newState, action);
  return newState;
}

export function savedGameState(): IGameState {
  return {
    resources: savedResources(),
    map: savedMap(),
    player: savedPlayer(),
    followers: savedFollowers(),
    time: savedTime(),
  }
}

export function initialGameState(): IGameState {
  return Object.assign({}, {
    resources: initialResources(),
    map: new GameMap().generateMap(),
    player: initialPlayer(),
    followers: initialFollowers(),
    time: initialTime(),
  });
}
