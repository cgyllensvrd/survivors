export default function(condition: boolean, data: string) {
  if (condition) return;
  let new_errors = [];
  const old_errors = window.localStorage.getItem('errors')
  if (old_errors) {
    new_errors = JSON.parse(old_errors);
  }
  new_errors.push(data);
  window.localStorage.setItem('errors', JSON.stringify(new_errors));
}
