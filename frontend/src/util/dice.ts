() => {
  let total = 0;
  let dice = 0;
  do {
    dice = Math.floor(Math.random() * 6) + 1;
    total += dice;
  } while (dice == 6);
  return total;
}
