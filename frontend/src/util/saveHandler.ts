import { Decoder } from 'type-safe-json-decoder'

export default function saveHandler<T>(initialState: T, decoder: Decoder<T>, name: string): T {
  // Applying initialState first, and the save after, ensures any new
  // resources are added in their initial resources, allowing for an easy way
  // to "upgrade" existing saves with new content.

  let save = {};
  // window unavailable during testing.
  if (typeof window === 'undefined') {
    return initialState;
  }

  try {
    const storedSave = window.localStorage.getItem('save');
    if (storedSave === null) {
      return initialState;
    }
    save = decoder.decodeJSON(storedSave);
  } catch (e) {
    console.log("Failed to parse " + name);
    return initialState;
  }

  return Object.assign({}, initialState, save);
}
