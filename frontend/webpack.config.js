'use strict';
const path = require('path');

const dir = path.resolve(__dirname, 'web/dist');

module.exports = {
  watch: true,
  entry: './src/main.tsx',
  devtool: 'source-map',
  module: {
    rules: [
      { test: /\.ts(x?)$/, loader: 'ts-loader', exclude: [ '/node_modules/', '/typings/'] }
    ]
  },
  output: {
    path: dir,
    filename: 'app.js'
  },
  resolve: {
    extensions: [ '.js', '.jsx', '.ts', '.tsx']
  }
};
