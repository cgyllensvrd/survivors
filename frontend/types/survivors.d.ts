interface FSAction {
  type: string,
  payload?: any,
  error?: boolean
}

interface IFollowers {
  count: number,
}

interface IResources {
  [ key:string]: IResource
  wood:     IResource,
  safety:   IResource,
  prestige: IResource,
}

interface ResourceListProps {
  res: IResources;
  cell: number;
  followers: IFollowers;
  time: ITime;
}

interface IResource {
  amount: number;
}

interface IMapData {
  tiles: Array<Array<number>>;
  width: number;
  height: number;
}

interface IPlayer {
  coords: {
    x: number;
    y: number;
  }
}

interface ITime {
  day: number;
  msecForLastDayIncrement: number;
}

interface IGameState {
  resources: IResources;
  map: IMapData;
  player: IPlayer;
  followers: IFollowers;
  time: ITime;
}

interface ButtonProps {
  label: string;
  action: () => void;
  disabled?: boolean;
}


