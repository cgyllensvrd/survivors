'use strict';
var dir = 'web/dist/main';

module.exports = {
  watch: true,
  entry: './src/main.js',
  module: {
    loaders: [
      { test: /\.js$/, loaders: ['babel-loader'], exclude: /node_modules/ }
    ]
  },
  output: {
    path: dir,
    filename: 'app.js'
  },
  resolve: {
    extensions: ['', '.js']
  }
};
